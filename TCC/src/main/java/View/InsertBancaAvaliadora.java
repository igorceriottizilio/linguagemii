package View;

import Controller.Run;
import DAO.BancaAvaliadoraDAO;
import DAO.ProfessorDAO;
import DAO.PropostaDAO;
import Entity.Professor;
import Entity.BancaAvaliadora;
import Entity.Proposta;
import Exception.EntityNotRegisteredException;
import Exception.NotAvailableException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import javax.swing.JOptionPane;

/**
 *
 * @author igorc
 */
public class InsertBancaAvaliadora extends Display implements Run {

    private Proposta proposta;
    private final List<Professor> avaliadores;
    private final List<Professor> todosProfessores;
    private final List<String> professoresList;
    private final List<String> propostasList;
    private final List<Proposta> todasPropostas;
    private final PropostaDAO propostaDAO;
    private final ProfessorDAO professorDAO;
    private final BancaAvaliadoraDAO bancaAvaliadoraDAO;

    public InsertBancaAvaliadora() {
        attributes = new LinkedHashMap<>() {
            {
                put("proposta", null);
                put("avaliadores", new ArrayList<>());
            }
        };
        this.avaliadores = new ArrayList<>();
        this.professoresList = new ArrayList<>();
        this.propostasList = new ArrayList<>();
        this.todosProfessores = new ArrayList<>();
        this.todasPropostas = new ArrayList<>();
        this.propostaDAO = PropostaDAO.getInstance();
        this.professorDAO = ProfessorDAO.getInstance();
        this.bancaAvaliadoraDAO = BancaAvaliadoraDAO.getInstance();
    }

    @Override
    public void run() {
        attributes.forEach((key, value) -> addInfos(key, value));

        BancaAvaliadora bancaAvaliadora = new BancaAvaliadora(avaliadores, proposta.getOrientador(), proposta);
        this.proposta.setBancaAvaliadora(bancaAvaliadora);
        propostaDAO.edit(this.proposta);
       
        bancaAvaliadoraDAO.insert(bancaAvaliadora);
        HomePage.display();
    }

    public void addInfos(Object key, Object value) {
        boolean isProposta = key.toString().equals("proposta");
        boolean ok = false;

        if (isProposta) {
            do {
                try {
                    if (!propostaDAO.getCollection().isEmpty()) {

                        propostaDAO.getCollection().forEach(p -> todasPropostas.add(((Proposta) p)));

                        if (!bancaAvaliadoraDAO.getCollection().isEmpty()) {

                                bancaAvaliadoraDAO.getCollection()
                                    .stream()
                                    .map(b -> ((BancaAvaliadora) b).getProposta())
                                    .collect(Collectors.toList())
                                    .stream()
                                    .filter(p -> todasPropostas.contains(p))
                                    .forEach(prop -> todasPropostas.remove(prop));

                            if (!todasPropostas.isEmpty()) {
                                todasPropostas.forEach(p -> propostasList.add(p.getTitulo().keySet().iterator().next()));
                            } else {
                                throw new NotAvailableException("N\u00E3o foram encontrados propostas dispon\u00EDveis! Todas j\u00E1 est\u00E3o vinculadas a bancas!");
                            }

                        } else {
                            todasPropostas.forEach(proposta -> propostasList.add(proposta.getTitulo().keySet().iterator().next()));
                        }

                    } else {
                        throw new EntityNotRegisteredException("N\u00E3o foram encontradas propostas dispon\u00EDveis! N\u00E3o h\u00E1 propostas cadastradas!");
                    }

                    Object propostaSelecionada = super.dropdDown( "Escolha a proposta:", "Propostas", propostasList);

                    ok = true;
                    this.proposta = propostaDAO.findPropostaByTitulo(propostaSelecionada.toString());

                } catch (NotAvailableException nae) {
                    JOptionPane.showMessageDialog(null, nae, "Error", JOptionPane.ERROR_MESSAGE);
                    HomePage.display();
                } catch (EntityNotRegisteredException enr) {
                    JOptionPane.showMessageDialog(null, enr, "Error", JOptionPane.ERROR_MESSAGE);
                    HomePage.display();
                } catch (NullPointerException npe) {
                    HomePage.display();
                }
            } while (ok != true);
        } else {
            do {
                try{
                if (!professorDAO.getCollection().isEmpty()) {

                    professorDAO.getCollection().forEach(p -> todosProfessores.add((Professor) p));

                    todosProfessores.stream()
                            .filter(prof -> this.proposta.getOrientador().getGrandeArea().equals(prof.getGrandeArea())
                            && !this.proposta.getOrientador().equals(prof))
                            .collect(Collectors.toList())
                            .forEach(p -> professoresList
                            .add(p.getNome() + " - " + " Orienta: "
                                    + p.getProjetosOrientador().size()
                                    + " - " + "Avalia: " + p.getProjetosAvaliador().size()
                                    + " - " + p.getEmail()));

                } else {
                    throw new EntityNotRegisteredException("N\u00E3o foram encontrados professores dispon\u00EDveis! N\u00E3o h\u00E1 professores cadastrados!");
                }

                Integer tentativa = 0;

                if (professoresList.size() < 2) {
                    JOptionPane.showMessageDialog(null, "N\u00E3o foi encontrado número mínimo de professores para serem avaliadores! Necessários 2 - Encontrados:  " + String.valueOf(professoresList.size()), "Error", JOptionPane.ERROR_MESSAGE);
                    HomePage.display();
                } else {
                    do {
                        String iteracao = tentativa.equals(1) ? "segundo " : "primeiro ";
                        
                        Object professorSelecionado = super.dropdDown("Foram encontrados os professores abaixo para compor a banca avaliadora. Selecione o "
                                + iteracao + "avaliador:", "Avaliadores", professoresList);

                        Professor professorToAdd = professorDAO.findProfessorByNome(Arrays.asList(professorSelecionado.toString().split(" - ")).get(0));
                        professorToAdd.setProjetosAvaliador(proposta);
                        proposta.setAvaliadores(professorToAdd);
                        avaliadores.add(professorToAdd);
                        professoresList.remove(professorSelecionado.toString());

                        ok = true;
                        tentativa++;
                    } while (tentativa < 2);
                }
                } catch (EntityNotRegisteredException enr) {
                    JOptionPane.showMessageDialog(null, enr, "Error", JOptionPane.ERROR_MESSAGE);
                    HomePage.display();
                } catch (NullPointerException npe) {
                    HomePage.display();
                }
            } while (!ok);
        }
    }    

    public Proposta getProposta() {
        return proposta;
    }

    public void setProposta(Proposta proposta) {
        this.proposta = proposta;
    }
}
