package Interface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author igorc
 */
public interface ProfessorInfos {

    Map<String, List<String>> areas = new HashMap<>() {
        {
            put("TI", new ArrayList<>(Arrays.asList("Arquitetura de computadores", "Linguagem de Programacao", "Interface homem-computador")));
            put("SAUDE", new ArrayList<>(Arrays.asList("Citologia", "Bioquimica")));
            put("ADMINISTRACAO", new ArrayList<>(Arrays.asList("Biblioteconomia", "Economia")));
        }
    };

    List<String> projetosCampus = new ArrayList<String>() {
        {
            add("Laboratório de prototipagem para ensino de ciências");
            add("Desenvolvimento de instrumentos meteorológicos caseiros");
            add("O Ensino de História através dos games");
            add("Estudos em Software e Hardware Livre");
            add("Inovações em linguagem Java - introdução do paradigma de programação funcional");
            add("Soluções de infraestrutura para repositórios corporativos");
            add("Testes de Software utilizando novas funcionalidades de Selenium WebDriver");
            add("Formação integral pela experimentação em Química");
            add("Caracterização e avaliação potencial biotecnológico e funcional de micro-organismos endofíticos");
        }
    };

}
