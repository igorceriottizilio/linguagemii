
package DAO;

import Interface.DaoInterface;
import java.util.ArrayList;

import Entity.Avaliacao;
import Entity.BancaAvaliadora;
import java.util.List;

/**
 *
 * @author igorc
 */
public class AvaliacaoDAO implements DaoInterface{
    private List<Object> collection;
    private static AvaliacaoDAO avaliacoes = null;
    
    public AvaliacaoDAO(List<Object> collection){
        this.collection = collection;
        avaliacoes = this;
    }
    
    public static AvaliacaoDAO getInstance() {
        return avaliacoes;
    }
    
     public List<Object> getCollection() {
        return collection;
    }


    @Override
    public void insert(Object obj) {
        this.getCollection().add(obj);
    }

    @Override
    public void delete(Object obj) {
        getCollection().remove(obj);
    }

    @Override
    public void edit(Object newObj) {
        Avaliacao newAvaliacao = (Avaliacao) newObj;
        Avaliacao oldAvaliacao = findByBanca(newAvaliacao.getBancaAvaliadora());
        oldAvaliacao.setAvaliacao(newAvaliacao);

    }
    
    public Avaliacao findByBanca(BancaAvaliadora bancaAvaliadora){
        List<Avaliacao> tempAvaliacao = new ArrayList<>();
        
        for(Object avaliacao : getCollection()){
            tempAvaliacao.add((Avaliacao) avaliacao);
        }
        
        return tempAvaliacao.stream().filter(avaliacao -> avaliacao.getBancaAvaliadora().equals(bancaAvaliadora)).findFirst().orElse(null);
    }
}
