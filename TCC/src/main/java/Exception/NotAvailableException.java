
package Exception;

/**
 *
 * @author igorc
 */
public class NotAvailableException extends Exception{
    public NotAvailableException(){}
    
    public NotAvailableException(String message){
        super(message);
    }
}
