
package Entity;

import java.util.List;

/**
 *
 * @author igorc
 */
public class BancaAvaliadora {
    private List<Professor> avaliadores;
    private Professor orientador;
    private Proposta proposta;
    
    public BancaAvaliadora(List<Professor> avaliadores, Professor orientador, Proposta proposta){
        this.avaliadores = avaliadores;
        this.orientador = orientador;
        this.proposta = proposta;
    }
    
     public void setBancaAvaliadora(BancaAvaliadora bancaAvaliadora){
        this.avaliadores = bancaAvaliadora.getAvaliadores();
        this.orientador = bancaAvaliadora.getOrientador();
        this.proposta = bancaAvaliadora.getProposta();
    }

    public List<Professor> getAvaliadores() {
        return avaliadores;
    }

    public void setAvaliadores(Professor avaliador) {
        this.avaliadores.add(avaliador);
    }

    public Professor getOrientador() {
        return orientador;
    }

    public void setOrientador(Professor orientador) {
        this.orientador = orientador;
    }

    public Proposta getProposta() {
        return proposta;
    }

    public void setProposta(Proposta proposta) {
        this.proposta = proposta;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof BancaAvaliadora == false) {
            return false;
        }
        BancaAvaliadora other = (BancaAvaliadora) obj;
        if (this.proposta == null) {
            return false;
        } else {
            return ((this.proposta.equals(other.proposta)));
        }
    }
    
    @Override
    public String toString() {
        return ("Avaliadores da banca : " +  getAvaliadores().get(0).getNome() + " - " + getAvaliadores().get(1).getNome()
                + "<br> Orientador da banca: " + orientador.getNome()
                + "<br> Proposta:  " + proposta.getTitulo().keySet().iterator().next()
                );
    }
}
