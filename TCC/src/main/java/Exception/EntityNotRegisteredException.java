/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exception;

/**
 *
 * @author igorc
 */
public class EntityNotRegisteredException extends Exception{
     public EntityNotRegisteredException(){}
    
    public EntityNotRegisteredException(String message){
        super(message);
    }
    
}
