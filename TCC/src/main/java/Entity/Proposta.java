
package Entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author igorc
 */
public class Proposta implements Serializable{
    private Map<String, String> titulo;
    private String descricao;
    private Aluno autor;
    private Professor orientador;
    private List<Professor> avaliadores;
    private BancaAvaliadora bancaAvaliadora;
    private Avaliacao avaliacao;

    public Proposta(Map<String, String> titulo, String descricao, Aluno autor, Professor orientador) {
        this.titulo = titulo;
        this.descricao = descricao;
        this.autor = autor;
        this.orientador = orientador;
        this.avaliadores = new ArrayList<>();
    }
    
    public void setProposta(Proposta proposta){
        this.titulo = proposta.getTitulo();
        this.descricao = proposta.getDescricao();
        this.autor = proposta.getAutor();
        this.orientador = proposta.getOrientador();
        this.avaliadores = proposta.getAvaliadores();
        this.bancaAvaliadora = proposta.getBancaAvaliadora();
        this.avaliacao = proposta.getAvaliacao();
    }


    public Map<String, String> getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo, String sugestaoProposta) {
        this.titulo.putIfAbsent(titulo, sugestaoProposta);
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Aluno getAutor() {
        return autor;
    }

    public void setAutor(Aluno autor) {
        this.autor = autor;
    }

    public Professor getOrientador() {
        return orientador;
    }

    public void setOrientador(Professor orientador) {
        this.orientador = orientador;
    }

    public List<Professor> getAvaliadores() {
        return avaliadores;
    }

    public void setAvaliadores(Professor avaliador) {
        this.avaliadores.add(avaliador);
    }

    public BancaAvaliadora getBancaAvaliadora() {
        return bancaAvaliadora;
    }

    public void setBancaAvaliadora(BancaAvaliadora bancaAvaliadora) {
        this.bancaAvaliadora = bancaAvaliadora;
    }

    public Avaliacao getAvaliacao() {
        return avaliacao;
    }

    public void setAvaliacao(Avaliacao avaliacao) {
        this.avaliacao = avaliacao;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof Proposta == false) { 
            return false;
        }
        Proposta other = (Proposta) obj;
        if (this.titulo == null)  
        {
            return false;
        } else {
            return ((this.titulo.equals(other.titulo)));
        }
    }
    
    @Override
    public String toString() {
        return ("Titulo: " + titulo
                + "<br> Descricao: " + descricao
                + "<br> Autor : " + autor.getNome()
                + "<br> Orientador: " + orientador.getNome() + " - " + orientador.getEmail()
                + "<br> Banca Avaliadora: " + bancaAvaliadora == null ? "Sem banca avaliadora cadastrada!" : bancaAvaliadora.getAvaliadores().get(0).getNome()
                + "<br> Avaliacao: " + avaliacao == null ? "Sem avaliacao cadastrada!" : avaliacao.getNotaFinal().toString()
                );
    }
}
