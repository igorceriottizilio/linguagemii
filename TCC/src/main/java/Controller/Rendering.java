
package Controller;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author igorc
 */
public class Rendering {
    private static HashMap options;

    static {
        Rendering.options = new HashMap<String, String>() {{
                put("1", "View.InsertAluno");
                put("2", "View.InsertProfessor");
                put("3", "View.InsertProposta");
                put("4", "View.InsertBancaAvaliadora");
                put("5", "View.InsertAvaliacao");
                put("6", "View.ListPropostas");
        }};
    }

    public static void directTo(String cmd) {
        String actionClass = (String) options.get(cmd);
        try {
            Class classe = Class.forName(actionClass);
            Run run = (Run) classe.newInstance();
            run.run();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Rendering.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException iex) {
            Logger.getLogger(Rendering.class.getName()).log(Level.SEVERE, null, iex);
        } catch (IllegalAccessException iaex) {
            Logger.getLogger(Rendering.class.getName()).log(Level.SEVERE, null, iaex);
        } catch (NullPointerException ex) {
             JOptionPane.showMessageDialog(null, "Informe uma operação entre 1 e 6!"); 
        }
    }
}