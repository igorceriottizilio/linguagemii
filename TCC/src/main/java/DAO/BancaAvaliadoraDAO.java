
package DAO;

import Entity.BancaAvaliadora;
import Entity.Proposta;
import Interface.DaoInterface;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author igorc
 */
public class BancaAvaliadoraDAO implements DaoInterface{
    private List<Object> collection;
    private static BancaAvaliadoraDAO bancas = null;
    
    public BancaAvaliadoraDAO(List<Object> collection){
        this.collection = collection;
        bancas = this;
    }
    
    public static BancaAvaliadoraDAO getInstance() {
        return bancas;
    }
    
     public List<Object> getCollection() {
        return collection;
    }

    @Override
    public void insert(Object obj) {
        this.getCollection().add(obj);
    }

    @Override
    public void delete(Object obj) {
        getCollection().remove(obj);
    }

    @Override
    public void edit(Object newObj) {
        BancaAvaliadora newBanca = (BancaAvaliadora) newObj;
        BancaAvaliadora oldBanca = findByProposta(newBanca.getProposta());
        oldBanca.setBancaAvaliadora(newBanca);

    }

    public BancaAvaliadora findByProposta(Proposta prosposta){
        List<BancaAvaliadora> tempBanca = new ArrayList<>();
        
        for(Object banca : getCollection()){
            tempBanca.add((BancaAvaliadora) banca);
        }
  
        return tempBanca.stream().filter(banca -> banca.getProposta().equals(prosposta)).findFirst().orElse(null);
    }
    
}
