package DAO;

import Entity.Professor;
import Interface.DaoInterface;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
 *
 * @author igorc
 */
public class ProfessorDAO implements DaoInterface{
    private ArrayList collection;
    private static ProfessorDAO professores = null;
    
    public ProfessorDAO(ArrayList collection){
        this.collection = collection;
        professores = this;
    }
    
    public static ProfessorDAO getInstance() {
        return professores;
    }
            
    @Override
    public void insert(Object obj) {
        getCollection().add(obj);
    }

    @Override
    public void delete(Object obj) {
        getCollection().remove(obj);
    }

    @Override
    public void edit(Object newObj) throws ClassCastException{
        Professor newProfessor = (Professor) newObj;
        Professor oldProfessor = findProfessorByNome(newProfessor.getNome());
        oldProfessor.setProfessor(newProfessor);
    }

    public ArrayList getCollection() {
        return collection;
    }
    
    public Professor findProfessorByNome(String nome){
        List<Professor> tempProfessores = new ArrayList<>();
        
        for(Object professor : getCollection()){
            tempProfessores.add((Professor) professor);
        }
  
        return tempProfessores.stream().filter(professor -> professor.getNome().equals(nome)).findFirst().orElse(null);
    }
    
    public List<Professor> findByArea(String grandeArea){
        List<Professor> tempProfessores = new ArrayList<>();
        
        for(Object professor : getCollection()){
            tempProfessores.add((Professor) professor);
        }
        
        return tempProfessores
                .stream()
                .filter(p -> p.getGrandeArea().equals(grandeArea))
                .collect(Collectors.toList());
    }
    
}
