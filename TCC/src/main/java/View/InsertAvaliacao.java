package View;

import Controller.Run;
import Entity.BancaAvaliadora;
import Entity.Professor;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import javax.swing.JOptionPane;

import DAO.BancaAvaliadoraDAO;
import DAO.PropostaDAO;
import DAO.AvaliacaoDAO;
import Entity.Avaliacao;
import Exception.EntityNotRegisteredException;
import Exception.NotAvailableException;
import java.util.stream.Collectors;

/**
 *
 * @author igorc
 */
public class InsertAvaliacao extends Display implements Run {

    private BancaAvaliadora bancaAvaliadora;
    private final List<Double> notas;
    private Double notaFinal;
    private List<Professor> professores;
    private final BancaAvaliadoraDAO bancaAvaliadoraDAO;
    private final PropostaDAO propostaDAO;
    private final AvaliacaoDAO avaliacaoDAO;

    public InsertAvaliacao() {
        this.professores = new ArrayList<>();
        this.notas = new ArrayList<>();
        this.notaFinal = 0.0;
        this.bancaAvaliadoraDAO = BancaAvaliadoraDAO.getInstance();
        this.propostaDAO = PropostaDAO.getInstance();
        this.avaliacaoDAO = AvaliacaoDAO.getInstance();
        attributes = new LinkedHashMap<Object, Object>() {
            {
                put("proposta", null);
                put("nota", null);
            }
        };
    }

    @Override
    public void run() {
        attributes.forEach((key, value) -> addInfos(key, value));

        Avaliacao avaliacao = new Avaliacao(bancaAvaliadora, professores, notas);
        avaliacao.setNotaFinal(notaFinal);
        
        bancaAvaliadora.getProposta().setAvaliacao(avaliacao);
        propostaDAO.edit(bancaAvaliadora.getProposta());
        
        avaliacaoDAO.insert(avaliacao);
        HomePage.display();
    }

    public void addInfos(Object key, Object value) {
        boolean isProposta = key.toString().equals("proposta");
        boolean ok = false;
        boolean notaOk = false;

        if (isProposta) {
            do {
                try {
                    List<String> propostasBancas = new ArrayList<>();
                    List<BancaAvaliadora> todasBancas = new ArrayList<>();

                    if (!bancaAvaliadoraDAO.getCollection().isEmpty()) {

                        bancaAvaliadoraDAO.getCollection().forEach(bancaObj -> todasBancas.add((BancaAvaliadora) bancaObj));

                        if (!avaliacaoDAO.getCollection().isEmpty()) {

                            avaliacaoDAO
                                    .getCollection()
                                    .stream()
                                    .map(a -> ((Avaliacao) a).getBancaAvaliadora())
                                    .collect(Collectors.toList())
                                    .stream()
                                    .filter(b -> todasBancas.contains(b))
                                    .forEach(sameBanca -> todasBancas.remove(sameBanca));

                            if (!todasBancas.isEmpty()) {
                                todasBancas.forEach(banca -> propostasBancas.add(banca.getProposta().getTitulo().keySet().iterator().next()));
                            } else {
                                throw new NotAvailableException("N\u00E3o foram encontradas bancas dispon\u00EDveis! Todas j\u00E1 tiverem avaliações cadastradas!");
                            }

                        } else {
                            todasBancas.forEach(banca -> propostasBancas.add(banca.getProposta().getTitulo().keySet().iterator().next()));
                        }

                        String propostaSelecionada = super.dropdDown("Escolha a proposta:", "Propostas", propostasBancas).toString();
                        ok = true;
                        this.bancaAvaliadora = bancaAvaliadoraDAO.findByProposta(propostaDAO.findPropostaByTitulo(propostaSelecionada));
                        this.professores = bancaAvaliadora.getAvaliadores();
                        this.professores.add(bancaAvaliadora.getOrientador());

                        if (propostaSelecionada.isEmpty()) {
                            ok = false;
                        }
                    } else {
                        throw new EntityNotRegisteredException("N\u00E3o foram encontradas bancas dispon\u00EDveis! Não há bancas cadastradas!");
                    }
                } catch (NotAvailableException nae) {
                    JOptionPane.showMessageDialog(null, nae, "Error", JOptionPane.ERROR_MESSAGE);
                    HomePage.display();
                } catch (EntityNotRegisteredException enr) {
                    JOptionPane.showMessageDialog(null, enr, "Error", JOptionPane.ERROR_MESSAGE);
                    HomePage.display();
                } catch (NullPointerException npe) {
                    HomePage.display();
                }
            } while (!ok);

        } else {
            do {
                List<String> todosProfessores = new ArrayList<>();
                bancaAvaliadora.getAvaliadores().forEach(a -> todosProfessores.add(a.getNome()));

                do {
                    String professorSelecionado = super.dropdDown("Escolha o professor para informar a nota :", "Professores", todosProfessores).toString();
                    todosProfessores.remove(professorSelecionado);

                    do {
                        try {
                            Double nota = Double.valueOf(JOptionPane.showInputDialog(null, "Informe nota (utilize ponto!)"));
                            
                            if(nota/10.0 > 1.0){
                                throw new NumberFormatException();
                            }
                            
                            notas.add(nota);
                            notaOk = true;
                            
                        } catch (NumberFormatException nfe) {
                            JOptionPane.showMessageDialog(null, "Informe uma nota válida (entre 1 e 10)!");
                            notaOk = false;
                        } catch (NullPointerException npe) {
                            HomePage.display();
                        }
                    } while (!notaOk);

                } while (!todosProfessores.isEmpty());

                ok = true;

            } while (!ok);

            notaFinal = calculaMedia(notas);
            if ( notaFinal >= 7.0) {
                JOptionPane.showMessageDialog(null, "Proposta APROVADA!");
            } else {
                JOptionPane.showMessageDialog(null, "Proposta REPROVADA!");
            }
        }
    }

    public Double calculaMedia(List<Double> notas) {
        Double soma = 0.0;
        for (Double nota : notas) {
            soma += nota;
        }
        return soma / 3.0;
    }

    public BancaAvaliadora getBancaAvaliadora() {
        return bancaAvaliadora;
    }

    public void setBancaAvaliadora(BancaAvaliadora bancaAvaliadora) {
        this.bancaAvaliadora = bancaAvaliadora;
    }

    public Double getNotaFinal() {
        return notaFinal;
    }

    public void setNotaFinal(Double notaFinal) {
        this.notaFinal = notaFinal;
    }

    public List<Professor> getProfessores() {
        return professores;
    }

    public void setProfessores(Professor professor) {
        this.professores.add(professor);
    }
    
    
}
