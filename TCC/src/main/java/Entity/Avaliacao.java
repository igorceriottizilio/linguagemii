package Entity;

import java.util.LinkedHashMap;
import java.util.List;

/**
 *
 * @author igorc
 */
public class Avaliacao {

    private BancaAvaliadora bancaAvaliadora;
    private LinkedHashMap<List<Professor>, List<Double>> notasProfessores;
    private List<Integer> notas;
    private Double notaFinal;

    public Avaliacao(BancaAvaliadora bancaAvaliadora, List<Professor> professores, List<Double> notas) {
        this.bancaAvaliadora = bancaAvaliadora;
        this.notasProfessores = new LinkedHashMap<>() {
            {
                put(professores, notas);
            }
        };
    }

    public void setAvaliacao(Avaliacao avaliacao) {
        this.bancaAvaliadora = avaliacao.getBancaAvaliadora();
        this.notasProfessores = avaliacao.getNotasProfessores();
        this.notas = avaliacao.getNotas();
        this.notaFinal = avaliacao.getNotaFinal();
    }

    public BancaAvaliadora getBancaAvaliadora() {
        return bancaAvaliadora;
    }

    public void setBancaAvaliadora(BancaAvaliadora bancaAvaliadora) {
        this.bancaAvaliadora = bancaAvaliadora;
    }

    public List<Integer> getNotas() {
        return notas;
    }

    public void setNotas(List<Integer> notas) {
        this.notas = notas;
    }

    public Double getNotaFinal() {
        return notaFinal;
    }

    public void setNotaFinal(Double notaFinal) {
        this.notaFinal = notaFinal;
    }

    public LinkedHashMap<List<Professor>, List<Double>> getNotasProfessores() {
        return notasProfessores;
    }

    public void setNotasProfessores(List<Professor> professores, List<Double> notas) {
        this.notasProfessores.put(professores, notas);
    }

    @Override
    public String toString() {
        StringBuilder notaProfessor = new StringBuilder();

        for (List<Professor> professores : notasProfessores.keySet()) {
            for (int i = 0; i < professores.size(); i++) {
                StringBuilder professor = new StringBuilder(professores.get(i).getNome());
                StringBuilder nota = new StringBuilder(String.valueOf(notasProfessores.get(professores).get(i)));
                notaProfessor.append(" [ Nota " + professor.toString() + ": " + nota.toString() + " ] <br> " );
            }
        }

        return (notaProfessor.toString() +
               String.format("<br>Nota final: %.2f", notaFinal));
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof Avaliacao == false) {
            return false;
        }
        Avaliacao other = (Avaliacao) obj;
        if (this.bancaAvaliadora == null) {
            return false;
        } else {
            return ((this.bancaAvaliadora.equals(other)));
        }
    }
}
