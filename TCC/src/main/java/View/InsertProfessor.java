package View;

import Controller.Run;
import DAO.ProfessorDAO;
import Entity.Professor;
import Exception.EmptyFieldException;
import Interface.ProfessorInfos;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author igorc
 */
public class InsertProfessor extends Display implements Run, ProfessorInfos {

    private String nome;
    private String email;
    private String projetoCampus;
    private final List<String> sugestoesProjeto = new ArrayList<>();
    private String areaInteresse;

    public InsertProfessor() {

        attributes = new LinkedHashMap<Object, Object>() {
            {
                put("nome", null);
                put("email", null);
                put("areaInteresse", null);
                put("projetoCampus", null);
                put("sugestão de projeto", null);
            }
        };
        
        this.projetoCampus = null;
    }

    @Override
    public void run() {
        ProfessorDAO professorDao = ProfessorDAO.getInstance();

        attributes.forEach((key, value) -> addInfos(key, value));

        Professor professor = new Professor(nome, email, sugestoesProjeto, areaInteresse);
        professor.setProjetoCampus(this.projetoCampus);
        professorDao.insert(professor);
        HomePage.display();
    }

    public void addInfos(Object key, Object value) {
        boolean ok = false;
        boolean isArea = key.equals("areaInteresse");
        boolean isProjetoCampus = key.equals("projetoCampus");

        if (!isArea && !isProjetoCampus) {
            do {
                if (key.toString().contains("projeto")) {
                    boolean lastProposta = false;
                    Integer iteracao = 0;
                    do {
                        if (iteracao == 0) {
                            try {
                                String proposta = JOptionPane.showInputDialog(null, "Informe " + key + " do professor");
                                if (proposta.isEmpty()) {
                                    throw new EmptyFieldException("Por favor, informe uma sugestão de proposta!");
                                } else {
                                    sugestoesProjeto.add(proposta);
                                }
                            } catch (EmptyFieldException e) {
                                lastProposta = false;
                            } catch (NullPointerException e) {
                                HomePage.display();
                            }

                        } else {
                            Object[] opcoes = {"Sim", "Não"};
                            int opcaoSelecionada = JOptionPane.showOptionDialog(null,
                                    "Gostaria de adicionar mais uma proposta?",
                                    "Adicionar proposta adicional",
                                    JOptionPane.YES_NO_OPTION,
                                    JOptionPane.QUESTION_MESSAGE,
                                    null,
                                    opcoes,
                                    opcoes[0]);
                            if (!(opcaoSelecionada == JOptionPane.OK_OPTION)) {
                                lastProposta = true;
                                ok = true;
                            } else {
                                try {
                                    String proposta = JOptionPane.showInputDialog(null, "Informe " + key + " do professor");
                                    if (proposta.isEmpty()) {
                                        throw new EmptyFieldException("Por favor, informe uma sugestão de proposta!");
                                    } else {
                                        sugestoesProjeto.add(proposta);
                                    }
                                } catch (EmptyFieldException e) {
                                    lastProposta = false;
                                } catch (NullPointerException e) {
                                    HomePage.display();
                                }
                            }
                        }
                        iteracao++;
                    } while (!lastProposta);
                } else {
                    try {
                        value = JOptionPane.showInputDialog(null, "Informe " + key + " do professor");
                        if (value.toString().isEmpty()) {
                            throw new EmptyFieldException("Um professor precisa ter " + key);
                        }
                        if (key.toString().contains("nome")) {
                            this.nome = value.toString();

                        } else if (key.toString().contains("email")) {
                            this.email = value.toString();
                        }

                        ok = true;
                    } catch (EmptyFieldException ex) {
                        JOptionPane.showMessageDialog(null, ex);
                        ok = false;
                    } catch (NullPointerException npe) {
                        HomePage.display();
                    }
                }
            } while (ok != true);

        } else if (isArea && !isProjetoCampus) {

            do {
                try {
                    List<String> areasList = new ArrayList<>();

                    areas
                            .forEach((grandeArea, subArea)
                                    -> subArea
                                    .forEach(area
                                            -> areasList.add(area)));

                    areaInteresse = super.dropdDown("Área de Interesse:", "Escolha a área de interesse", areasList).toString();

                    if (areaInteresse == null) {
                        ok = false;
                    } else {
                        ok = true;
                    }
                } catch (NullPointerException npe) {
                    HomePage.display();
                }
            } while (!ok);
        } else {
            Object[] opcoes = {"Sim", "Não"};
            int opcaoSelecionada = JOptionPane.showOptionDialog(null,
                    "Professor está associado a um projeto do Campus?",
                    "Adicionar projeto Campus",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    opcoes,
                    opcoes[0]);

            if ((opcaoSelecionada == JOptionPane.OK_OPTION)) {
                do {
                    try {
                        projetoCampus = super.dropdDown("Projetos encontrados do Campus:", "Escolha o projeto do Campus", projetosCampus).toString();

                        if (projetoCampus == null) {
                            ok = false;
                        } else {
                            ok = true;
                        }
                        
                        this.projetoCampus = projetoCampus;
                        
                    } catch (NullPointerException npe) {
                        HomePage.display();
                    }
                } while (!ok);
            }

        }
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAreaInteresse() {
        return areaInteresse;
    }

    public void setAreaInteresse(String areaInteresse) {
        this.areaInteresse = areaInteresse;
    }

}
