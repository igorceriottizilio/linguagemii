package Entity;

import java.io.Serializable;

/**
 *
 * @author igorc
 */
public class Aluno implements Serializable {

    private String nome;
    private Long numeroMatricula;
    private String email;
    private Long telefone;
    private Proposta projeto;

    public Aluno(String nome, Long numeroMatricula, String email, Long telefone) {
        this.nome = nome;
        this.numeroMatricula = numeroMatricula;
        this.email = email;
        this.telefone = telefone;
    }

    public void setAluno(Aluno aluno) {
        this.email = aluno.email;
        this.nome = aluno.nome;
        this.numeroMatricula = aluno.numeroMatricula;
        this.telefone = aluno.telefone;
        this.projeto = aluno.projeto;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Long getNumeroMatricula() {
        return numeroMatricula;
    }

    public void setNumeroMatricula(Long numeroMatricula) {
        this.numeroMatricula = numeroMatricula;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getTelefone() {
        return telefone;
    }

    public void setTelefone(Long telefone) {
        this.telefone = telefone;
    }

    public Proposta getProjeto() {
        return projeto;
    }

    public void setProjeto(Proposta projeto) {
        this.projeto = projeto;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof Aluno == false) {
            return false;
        }
        Aluno other = (Aluno) obj;
        if (this.nome == null) {
            return false;
        } else {
            return ((this.nome.equals(other.nome)));
        }
    }
    
    @Override
    public String toString() {
        return ("Nome do aluno: " + nome
                + "<br> Matricula: " + numeroMatricula
                + "<br> E-mail: " + email
                + "<br> Telefone: " + telefone);
    }
}
