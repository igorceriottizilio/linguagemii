package View;

import Controller.Run;
import DAO.PropostaDAO;
import Entity.Proposta;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author igorc
 */
public class ListPropostas implements Run {

    @Override
    public void run() {
        PropostaDAO propostaDAO = PropostaDAO.getInstance();

        if (propostaDAO.getCollection().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Por favor, cadastre propostas para listá-las!");
            HomePage.display();
        } else {
            List<Proposta> propostasCadastradas = new ArrayList<>();

            for (Object proposta : propostaDAO.getCollection()) {
                propostasCadastradas.add((Proposta) proposta);
            }

            StringBuilder builder = new StringBuilder();

            String appendString = "<h2 style='color: blue; font-family:georgia'><b><TITULO></b></h2>\n"
                    + "<div>\n"
                    + "<li style='font-family:georgia'><i>Descricao: </i>\n"
                    + "				<ul><DESCRICAO></ul>\n"
                    + "<li style='font-family:georgia'><i>Aluno: </i>\n"
                    + "				<ul><ALUNO></ul>\n"
                    + "<li style='font-family:georgia'><i>Orientador: </i>\n"
                    + "				<ul><ORIENTADOR></ul>\n"
                    + "<li style='font-family:georgia'><i>Banca Avaliadora: </i>\n"
                    + "				<ul><BANCA></ul>\n"
                    + "<li style='font-family:georgia'><i>Avaliacao: </i>\n"
                    + "				<ul><AVALIACAO></ul>\n"
                    + "<h4 <HIDDEN> style='color: <COLOR>; font-family:georgia'><STATUS></h4></li></ul>\n"
                    + "</div>\n"
                    + "<br>";

            for (Proposta proposta : propostasCadastradas) {
                String replaceAll = appendString.replaceAll("<TITULO>", proposta.getTitulo().keySet().iterator().next());
                String replaceAll1 = replaceAll.replaceAll("<DESCRICAO>", proposta.getDescricao());
                String replaceAll2 = replaceAll1.replaceAll("<ALUNO>", proposta.getAutor().toString());
                String replaceAll3 = replaceAll2.replaceAll("<ORIENTADOR>", proposta.getOrientador().toString());
                String replaceAll4 = replaceAll3.replaceAll("<BANCA>", proposta.getBancaAvaliadora() == null ? "Sem banca cadastrada!" : proposta.getBancaAvaliadora().toString());
                String replaceAll5 = replaceAll4.replaceAll("<AVALIACAO>", proposta.getAvaliacao() == null ? "Sem avaliacoes!" : proposta.getAvaliacao().toString() + "<br>");
                String replaceAll6 = replaceAll5.replaceAll("<HIDDEN>", proposta.getAvaliacao() == null ? "hidden" : "");
                String replaceAll7 = replaceAll6.replaceAll("<COLOR>", proposta.getAvaliacao() == null ? "" : proposta.getAvaliacao().getNotaFinal() >= 7.0 ? "green" : "red");
                String replaceAll8 = replaceAll7.replaceAll("<STATUS>", proposta.getAvaliacao() == null ? "" : proposta.getAvaliacao().getNotaFinal() >= 7.0 ? "Aprovada!" : "Reprovada!");

                builder.append(replaceAll8);
            }

            JLabel label = new JLabel("<html>" + builder.toString() + "</html>");;
            JScrollPane scroller = new JScrollPane(label, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            JTextArea textArea = new JTextArea();
            textArea.setLineWrap(true);
            textArea.setWrapStyleWord(true);
            scroller.setPreferredSize(new Dimension(700, 700));
            JOptionPane.showMessageDialog(null, scroller);

            HomePage.display();
        }

    }
}
