package Controller;

import DAO.AlunoDAO;
import DAO.AvaliacaoDAO;
import DAO.BancaAvaliadoraDAO;
import DAO.ProfessorDAO;
import DAO.PropostaDAO;
import Exception.EmptyFieldException;
import View.HomePage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author igorc
 */
public class Controller {

    private static String arquivoAlunos;
    private static AlunoDAO alunoDAO;
    private static String arquivoProfessores;
    private static ProfessorDAO professorDAO;
    private static String arquivoPropostas;
    private static PropostaDAO propostaDAO;
    private static String arquivoBancaAvaliadora;
    private static BancaAvaliadoraDAO bancaAvaliadoraDAO;
    private static String arquivoAvaliacoes;
    private static AvaliacaoDAO avaliacaoDAO;

    public static void main(String[] args) {
        ArrayList collection = null;

        try {
            arquivoAlunos = reader("Por favor, informe o nome do arquivo para serem adicionadas informações sobre os alunos");
            alunoDAO = FileManager.fileExists(arquivoAlunos) ? new AlunoDAO((ArrayList) FileManager.openFile(arquivoAlunos))
                    : new AlunoDAO(new ArrayList());

            arquivoProfessores = reader("Por favor, informe o nome do arquivo para serem adicionadas informações sobre os professores");
            professorDAO = FileManager.fileExists(arquivoProfessores) ? new ProfessorDAO((ArrayList) FileManager.openFile(arquivoProfessores))
                    : new ProfessorDAO(new ArrayList());

            arquivoPropostas = reader("Por favor, informe o nome do arquivo para serem adicionadas informações sobre as propostas");
            propostaDAO = FileManager.fileExists(arquivoPropostas) ? new PropostaDAO((ArrayList) FileManager.openFile(arquivoPropostas))
                    : new PropostaDAO(new ArrayList());

            arquivoBancaAvaliadora = reader("Por favor, informe o nome do arquivo para serem adicionadas informações sobre as bancas avaliadoras");
            bancaAvaliadoraDAO = FileManager.fileExists(arquivoBancaAvaliadora) ? new BancaAvaliadoraDAO((ArrayList) FileManager.openFile(arquivoBancaAvaliadora))
                    : new BancaAvaliadoraDAO(new ArrayList());

            arquivoAvaliacoes = reader("Por favor, informe o nome do arquivo para serem adicionadas informações sobre as avaliações");
            avaliacaoDAO = FileManager.fileExists(arquivoAvaliacoes) ? new AvaliacaoDAO((ArrayList) FileManager.openFile(arquivoAvaliacoes))
                    : new AvaliacaoDAO(new ArrayList());

        } catch (EmptyFieldException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(null, "Encerrando o programa ...");
            System.exit(0);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Falha ao tentar abrir o arquivo");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }

        HomePage.display();
    }

    public static String reader(String message) throws EmptyFieldException, NullPointerException {
        boolean isOk = false;
        do {
            try {
                String dado = JOptionPane.showInputDialog(null, message);

                if (dado.isEmpty()) {
                    throw new EmptyFieldException();
                }

                isOk = true;
                return dado;

            } catch (EmptyFieldException ef) {
                JOptionPane.showMessageDialog(null, "Por favor, preencha o campo!");
            }
        } while (!isOk);

        return null;
    }
}
