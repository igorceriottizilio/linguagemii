
package Entity;

import Interface.ProfessorInfos;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author igorc
 */
public class Professor implements Serializable, ProfessorInfos{
    private String nome;
    private String email;
    private List<String> sugestaoProjeto;
    private String areaInteresse;
    private String projetoCampus;
    private List<Proposta> projetosOrientador;
    private List<Proposta> projetosAvaliador;

    public Professor(String nome, String email, List<String> sugestaoProjeto, String areaInteresse) {
        this.nome = nome;
        this.email = email;
        this.sugestaoProjeto = sugestaoProjeto;
        this.areaInteresse = areaInteresse;
        this.projetosAvaliador = new ArrayList<>();
        this.projetosOrientador = new ArrayList<>();
        this.projetoCampus = null;
    }
    
    public void setProfessor(Professor professor){
        this.nome = professor.getNome();
        this.email = professor.getEmail();
        this.areaInteresse = professor.getAreaInteresse();
        this.sugestaoProjeto = professor.getSugestaoProjeto();
        this.projetosAvaliador = professor.getProjetosAvaliador();
        this.projetosOrientador = professor.getProjetosOrientador();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getSugestaoProjeto() {
        return sugestaoProjeto;
    }

    public void setSugestaoProjeto(String sugestaoProjeto) {
        this.sugestaoProjeto.add(sugestaoProjeto);
    }

    public String getAreaInteresse() {
        return areaInteresse;
    }

    public void setAreaInteresse(String areaInteresse) {
        this.areaInteresse = areaInteresse;
    }

    public List<Proposta> getProjetosOrientador() {
        return projetosOrientador;
    }

    public void setProjetosOrientador(Proposta projetoOrientador) {
        this.projetosOrientador.add(projetoOrientador);
    }

    public List<Proposta> getProjetosAvaliador() {
        return projetosAvaliador;
    }

    public void setProjetosAvaliador(Proposta projetoAvaliador) {
        this.projetosAvaliador.add(projetoAvaliador);
    }

    public String getProjetoCampus() {
        return projetoCampus;
    }

    public void setProjetoCampus(String projetoCampus) {
        this.projetoCampus = projetoCampus;
    }
    
    public String getGrandeArea(){
        return areas
                .entrySet()
                .stream()
                .filter(e -> e.getValue().contains(areaInteresse))
                .findFirst()
                .orElse(null)
                .toString();
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof Professor == false) { 
            return false;
        }
        Professor other = (Professor) obj;
        if (this.nome == null)  
        {
            return false;
        } else {
            return ((this.nome.equals(other.nome)));
        }
    }
    
    @Override
    public String toString() {
        String projetosCampus = projetoCampus == null ? "Sem projetos do Campus associados" : "Projeto do Campus associado: [ " + projetoCampus + " ]";
        String sugestoesProjeto = sugestaoProjeto.isEmpty() ? "" : sugestaoProjeto.toString();
        
        return ("Nome do professor: " + nome
                + "<br> Sugestoes de projeto: " + sugestoesProjeto
                + "<br> " + projetosCampus
                + "<br> E-mail: " + email
                + "<br> Area de interesse: " + areaInteresse
                );
    }
}
