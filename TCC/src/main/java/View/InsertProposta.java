package View;

import Controller.Run;
import DAO.AlunoDAO;
import DAO.ProfessorDAO;
import DAO.PropostaDAO;
import java.util.LinkedHashMap;
import Entity.Aluno;
import Entity.Professor;
import Entity.Proposta;
import Exception.EmptyFieldException;
import Exception.EntityNotRegisteredException;
import Exception.NotAvailableException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.swing.JOptionPane;

/**
 *
 * @author igorc
 */
public class InsertProposta extends Display implements Run {

    private final Map<String, String> tituloSugestao;
    private String descricao;
    private Aluno autor;
    private Professor orientador;

    public InsertProposta() {
        this.tituloSugestao = new HashMap<>();
        attributes = new LinkedHashMap<Object, Object>() {
            {
                put("autor", null);
                put("orientador", null);
                put("titulo", null);
                put("descricao", null);
            }
        };
    }

    @Override
    public void run() {
        ProfessorDAO professorDao = ProfessorDAO.getInstance();
        AlunoDAO alunoDAO = AlunoDAO.getInstance();
        PropostaDAO propostaDAO = PropostaDAO.getInstance();

        attributes.forEach((key, value) -> addInfos(key, value, professorDao, alunoDAO, propostaDAO));

        Proposta proposta = new Proposta(tituloSugestao, descricao, autor, orientador);

        this.orientador.setProjetosOrientador(proposta);
        this.autor.setProjeto(proposta);

        alunoDAO.edit(this.autor);
        professorDao.edit(this.orientador);

        propostaDAO.insert(proposta);
    }

    public void addInfos(Object key, Object value, ProfessorDAO professorDao, AlunoDAO alunoDAO, PropostaDAO propostaDAO) {
        boolean ok = false;
        boolean isAluno = key.toString().equals("autor");
        boolean isOrientador = key.toString().equals("orientador");
        boolean isTitulo = key.toString().equals("titulo");

        if (!isAluno && !isOrientador) {
            do {
                try {
                    if (isTitulo) {
                        String propostaSelecionada = super.dropdDown(
                                "Escolha a proposta:", "Propostas", orientador.getSugestaoProjeto()).toString();

                        value = JOptionPane.showInputDialog(null, "Informe " + key.toString() + " da proposta",
                                propostaSelecionada);

                        this.tituloSugestao.put(value.toString(), propostaSelecionada);

                    } else {
                        value = JOptionPane.showInputDialog(null, "Informe " + key.toString() + " da proposta");
                        this.descricao = value.toString();

                    }
                    if (value.toString().isEmpty()) {
                        throw new EmptyFieldException("Uma proposta precisa ter " + key.toString());
                    }
                    ok = true;
                } catch (EmptyFieldException ex) {
                    JOptionPane.showMessageDialog(null, ex);
                    ok = false;
                } catch (NullPointerException npe) {
                    HomePage.display();
                }
            } while (ok != true);

        } else if (isAluno) {
            do {
                try {
                    List<String> alunosList = new ArrayList<>();
                    List<Aluno> todosAlunos = new ArrayList<>();

                    if (!alunoDAO.getCollection().isEmpty()) {

                        alunoDAO.getCollection().forEach(alunoObj -> todosAlunos.add((Aluno) alunoObj));

                        if (!propostaDAO.getCollection().isEmpty()) {

                            propostaDAO
                                    .getCollection()
                                    .stream()
                                    .map(p -> ((Proposta) p).getAutor())
                                    .collect(Collectors.toList())
                                    .stream()
                                    .filter(a -> todosAlunos.contains(a))
                                    .forEach(a -> todosAlunos.remove(a));

                            if (!todosAlunos.isEmpty()) {
                                todosAlunos.forEach(aluno -> alunosList.add(aluno.getNome()));
                            } else {
                                throw new NotAvailableException("N\u00E3o foram encontrados alunos dispon\u00EDveis! Todos j\u00E1 est\u00E3o vinculados a projetos!");
                            }
                        } else {
                            todosAlunos.forEach(aluno -> alunosList.add(aluno.getNome()));
                        }
                    } else {
                        throw new EntityNotRegisteredException("N\u00E3o foram encontrados alunos dispon\u00EDveis! N\u00E3o h\u00E1 alunos cadastrados!");
                    }

                    String alunoSelecionado = super.dropdDown(
                            "Escolha o aluno:", "Alunos", alunosList).toString();

                    this.autor = alunoDAO.findAlunoByNome(alunoSelecionado.toString());

                    if (alunoSelecionado == null) {
                        ok = false;
                    } else {
                        ok = true;
                    }
                } catch (NotAvailableException nae) {
                    JOptionPane.showMessageDialog(null, nae, "Error", JOptionPane.ERROR_MESSAGE);
                    HomePage.display();
                } catch (EntityNotRegisteredException enr) {
                    JOptionPane.showMessageDialog(null, enr, "Error", JOptionPane.ERROR_MESSAGE);
                    HomePage.display();
                } catch (NullPointerException npe) {
                    HomePage.display();
                }
            } while (!ok);

        } else {
            do {
                try {
                    List<String> professoresList = new ArrayList<>();
                    List<Professor> todosProfessores = new ArrayList<>();

                    if (!professorDao.getCollection().isEmpty()) {

                        professorDao.getCollection().forEach(professorObj -> todosProfessores.add((Professor) professorObj));
                        List<Professor> tempProfessores = new ArrayList<>(todosProfessores);

                        if (!propostaDAO.getCollection().isEmpty()) {

                            List<String> tiulosSugestaoPropostas = new ArrayList<>();

                            propostaDAO.getCollection()
                                    .forEach(p -> tiulosSugestaoPropostas.add(((Proposta) p).getTitulo().values().iterator().next()));

                            for (String sugestao : tiulosSugestaoPropostas) {
                                for(Professor prof : todosProfessores){
                                    if(prof.getSugestaoProjeto().isEmpty()){
                                        tempProfessores.remove(prof);
                                    }
                                    
                                    if(prof.getSugestaoProjeto().contains(sugestao)){
                                        prof.getSugestaoProjeto().remove(sugestao);
                                        
                                        if(prof.getSugestaoProjeto().isEmpty()){
                                            tempProfessores.remove(prof);
                                        }
                                    }
                                }
                            }

                            if (!tempProfessores.isEmpty()) {
                                tempProfessores.forEach(professor -> professoresList.add(professor.getNome() + " - " + professor.getEmail()));
                            } else {
                                throw new NotAvailableException("N\u00E3o foram encontrados orientadores dispon\u00EDveis! Todos j\u00E1 est\u00E3o com propostas vinculadas a projetos!");
                            }
                        } else {
                            todosProfessores.forEach(professor -> professoresList.add(professor.getNome() + " - " + professor.getEmail()));
                        }
                    } else {
                        throw new EntityNotRegisteredException("N\u00E3o foram encontrados professores dispon\u00EDveis! N\u00E3o h\u00E1 professores cadastrados!");
                    }

                    Object professorSelecionado = super.dropdDown(
                            "Escolha o professor orientador:", "Professores", professoresList);

                    ok = true;
                    this.orientador = professorDao.findProfessorByNome(Arrays.asList(professorSelecionado.toString().split(" - ")).get(0));

                } catch (NotAvailableException nae) {
                    JOptionPane.showMessageDialog(null, nae, "Error", JOptionPane.ERROR_MESSAGE);
                    HomePage.display();
                } catch (EntityNotRegisteredException enr) {
                    JOptionPane.showMessageDialog(null, enr, "Error", JOptionPane.ERROR_MESSAGE);
                    HomePage.display();
                } catch (NullPointerException npe) {
                    HomePage.display();
                }
            } while (!ok);
        }
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Aluno getAutor() {
        return autor;
    }

    public void setAutor(Aluno autor) {
        this.autor = autor;
    }

    public Professor getOrientador() {
        return orientador;
    }

    public void setOrientador(Professor orientador) {
        this.orientador = orientador;
    }
}
