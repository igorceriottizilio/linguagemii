
package View;

import Controller.Run;
import DAO.AlunoDAO;
import Entity.Aluno;
import Exception.EmptyFieldException;
import java.util.LinkedHashMap;
import javax.swing.JOptionPane;

/**
 *
 * @author igorc
 */
public class InsertAluno extends Display implements Run {
            
        private String nome;
        private String email;
        private Long telefone;
        private Long numeroMatricula;

        public InsertAluno(){
           
           attributes = new LinkedHashMap<Object, Object>()
                {{
                     put("nome", nome);
                     put("email", email);
                     put("telefone", telefone);
                     put("numero da matricula", numeroMatricula);
                }};
        }
    
    @Override
    public void run(){
        AlunoDAO alunoDAO = AlunoDAO.getInstance();
        
        attributes.forEach((key, value) -> addInfos(key, value));
        
        Aluno aluno = new Aluno(nome, numeroMatricula, email, telefone);
        alunoDAO.insert(aluno);
        HomePage.display();
    }
    
    public void addInfos(Object key, Object value){
        boolean ok = false;
        do {
            try {                
                value = JOptionPane.showInputDialog(null, "Informe " + key + " do aluno");
                if (value.toString().isEmpty()) {
                    throw new EmptyFieldException("Um aluno precisa ter " + key);
                }
                
                if(key.toString().contains("nome")){
                    this.nome =  value.toString();
                } else if (key.toString().contains("telefone")){
                    Long tryTelefone = Long.valueOf(value.toString());
                    this.telefone = tryTelefone;
                } else if (key.toString().contains("email")){
                    this.email = value.toString();
                } else {
                    Long tryMatricula = Long.valueOf(value.toString());
                    this.numeroMatricula = tryMatricula;
                }
                
                ok = true;
            } catch (EmptyFieldException ex) {
                JOptionPane.showMessageDialog(null, ex);
                ok = false;
            } catch (NumberFormatException nfe) {
                JOptionPane.showMessageDialog(null, "Apenas números!");
            } catch (NullPointerException npe) {
                HomePage.display();
            }
        } while (ok != true);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getTelefone() {
        return telefone;
    }

    public void setTelefone(Long telefone) {
        this.telefone = telefone;
    }

    public Long getNumeroMatricula() {
        return numeroMatricula;
    }

    public void setNumeroMatricula(Long numeroMatricula) {
        this.numeroMatricula = numeroMatricula;
    }
    
    
}
