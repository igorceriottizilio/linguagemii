
package DAO;

import Entity.Proposta;
import Interface.DaoInterface;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author igorc
 */
public class PropostaDAO implements DaoInterface{
    private List<Object> collection;
    private static PropostaDAO propostas = null;
    
    public PropostaDAO(List<Object> collection){
        this.collection = collection;
        propostas = this;
    }
    
    public static PropostaDAO getInstance() {
        return propostas;
    }
    
    public List<Object> getCollection() {
        return collection;
    }

    @Override
    public void insert(Object obj) {
        getCollection().add(obj);
    }

    @Override
    public void delete(Object obj) {
        getCollection().remove(obj);
    }

    @Override
    public void edit(Object newObj) {
        Proposta newProposta = (Proposta) newObj;
        Proposta oldProposta = findPropostaByTitulo(newProposta.getTitulo().keySet().iterator().next());
        oldProposta.setProposta(newProposta);
    }
    
    
    public Proposta findPropostaByTitulo(String titulo){
        List<Proposta> tempPropostas = new ArrayList<>();
        
        for(Object proposta : getCollection()){
            tempPropostas.add((Proposta) proposta);
        }
  
        return tempPropostas.stream().filter(proposta -> proposta.getTitulo().keySet().iterator().next().equals(titulo)).findFirst().orElse(null);
            
    } 
    
}
