package View;

import java.util.LinkedHashMap;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author igorc
 */
public class Display {
    
    protected LinkedHashMap<Object, Object> attributes;
    
    public Object dropdDown(String message, String title, List<String> list){
        return 
                JOptionPane.showInputDialog(
                                null,
                                message + "\n",
                                title,
                                JOptionPane.QUESTION_MESSAGE,
                                null,
                                list.toArray(),
                                list.get(0)
                        );
    }

    public LinkedHashMap<Object, Object> getAttributes() {
        return attributes;
    }

    public void setAttributes(LinkedHashMap<Object, Object> attributes) {
        this.attributes = attributes;
    }
}
